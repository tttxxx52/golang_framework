該framework請搭配以下前端使用
https://gitlab.com/tttxxx52/vue_to_golang_framework.git

此Framework使用gin framework為基底以改寫成handler api

並且擁有以下功能:

1.action factory 進行路由。   詳情請至main.go 與 action_factory.go 查看

2.對mariadb / mysql 的 orm 使用方法。   詳情請至model內與/database/mysql/interface.go 查看

3.kafka(範例在：https://gitlab.com/tttxxx52/golang_kafka_server.git  與   https://gitlab.com/tttxxx52/golang_kafka_api.git) 

4.對redis的func使用  (堪用)

5.對mongoDB的func使用  (堪用)

6.各類工具。  詳細請至util查看

