package handler

import (
	"api/content"
	"api/model"
	"api/util"
	"encoding/json"
	"github.com/gin-gonic/gin"
)

type UserHandler content.Handler

type ReceivedData struct {
	Id int `json:"id"`
}

func (handler *UserHandler) GetUserInfoById(c *gin.Context) interface{} {
	var data ReceivedData
	var user model.User
	if err := json.Unmarshal([]byte(c.GetString("parameters")), &data); err != nil {
		return util.RS{Message: "", Status: false}
	} else {
		return user.GetUserInfoById(data.Id)
	}
}

func (handler *UserHandler) GetMyselfInfo(c *gin.Context) interface{} {
	var user model.User
	userId := c.GetInt("userId")
	return user.GetUserInfoById(userId)

}

func (handler *UserHandler) GetUserList(c *gin.Context) interface{} {
	var user model.User
	return user.GetUserList()
}
