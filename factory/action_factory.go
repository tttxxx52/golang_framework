package factory

import (
	"api/handler"
)

var (
	userHandler handler.UserHandler
)

// this verification auth token
var ActionFactoryAuth = map[string]interface{}{
	"GetMyselfInfo": &userHandler,
}

var ActionFactory = map[string]interface{}{
	//======================================================================
	//						Not Auth Handler Api
	//======================================================================
	"GetUserList":     &userHandler,
	"GetUserInfoById": &userHandler,
}
