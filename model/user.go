package model

import (
	"api/database/mysql"
	"api/util/log"
	"database/sql"
)

type User struct {
	Id   int    `table:"id"`
	Name string `table:"name"`
}

func (model *User) GetUserList() (dataList []map[string]interface{}) {
	var name string
	var id int
	mysql.Model(model).
		Select([]string{"id", "name"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &name))
			data := map[string]interface{}{
				"id":   id,
				"name": name,
			}
			dataList = append(dataList, data)
		})
	return dataList
}

func (model *User) GetUserInfoById(userId int) (dataList map[string]interface{}) {
	var name string
	var id int
	log.Error(mysql.Model(model).
		Select([]string{"id", "name"}).
		Where("id", "=", userId).
		Find().Scan(&id, &name))
	return map[string]interface{}{
		"id":   id,
		"name": name,
	}
}
